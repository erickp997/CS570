/**
 * Anthony Alvidrez
 * Erick Pulido
 * 3/12/19 
 */
#include <iostream>
#include <vector>
#include "structures.h"
using namespace std;
#define ADDRESS_LENGTH 32
/*
* Apply a bit mask and shift right to a given logical address. 
* Returns the page number and is used to access the page number of any level.
* Performs the bitwise and of the logical address and the mask, then shift the
* the output by the number of bits specified.
*/
unsigned int LogicalToPage(unsigned int LogicalAddress,
                           unsigned int Mask,
                           unsigned int Shift) {
	unsigned int logical_to_page = (LogicalAddress & Mask) >> Shift;
    return logical_to_page;
}
/**
 * This method creates an array that holds 
 * the proper bits for each level that is needed in out
 * pagetable. We use temporary variables as well as a left
 * shift in order to create our bitmask array.
 */ 
void generate_bitmask(int *bitmask, int level) {
    int shift = ADDRESS_LENGTH;
    int mask = 0;
    for(int i = 0; i < level; i++) {
        shift = shift - bitmask[i];
        mask = (1 << bitmask[i]) - 1;
        mask = mask << shift;
        bitmask[i] = mask;
    }
}

/**
 * This method is an array that will generate the proper
 * amount of shifts that correspond with each level
 * in pagetable. 
 */ 
void generate_shift(int *shift_array, int level) {
    int shift = ADDRESS_LENGTH;
    for(int i = 0; i < level; i++) {
        shift = shift - shift_array[i];
        shift_array[i] = shift;
    }
}

/**
 * This method is used to generate each entry count that
 * corresponds to each level in our pagetable, it 
 * accomplishes this by utilizing a left shift.
 */ 
void generate_entry_count(int *entry, std::vector<int> bits, int level) {
    for(int i = 0; i < level; i++) 
        entry[i] = 1 << bits[i];
}

/**
 * PageTable is our main data structure that is used in this
 * program. It contains pointers to the start of other levels
 * that also have a pointer that points back to it. The number of levels
 * that we are to create depends on the user's input that is read from
 * the command line. 
 */
PageTable::PageTable(int level, int bitmask[], int shift[], int entry[]) {
		bitmask_array = bitmask;
		shift_array = shift;
		entry_count = entry;
		root_node_ptr = new Level(0, this); // root level
}
/*
* Given the page table and a logical address, return the appropriate entry of
* the page table. 
*/
Map *PageTable::PageLookup(PageTable *Page_Table, 
								  unsigned int LogicalAddress) {
    unsigned int index_of_entry;
    int level = Page_Table->level_count;
    int depth = (int)Page_Table->root_node_ptr->get_depth();
    int *bitmask = Page_Table->bitmask_array;
    int *shift = Page_Table->shift_array;
    for(int i = 0; i < Page_Table->level_count; i++) {
        index_of_entry = LogicalToPage(LogicalAddress, bitmask[i], shift[i]);
        if(depth == level - 1) {
            Map *to_insert = Page_Table->root_node_ptr->current->next_map;
            // validate the entry
               if(to_insert[index_of_entry].is_valid_frame == false)
                    return nullptr;
               return to_insert;
        }
    }
}

int PageTable::get_level_count(PageTable *Page_Table) {
	return Page_Table->level_count;
}
/*
* Add new entries to the page table. Frame is the frame index which corresponds
* to the logical address. Can replace void to int or bool for error code if
* memory is unable to be allocated.
*/
void PageTable::PageInsert(PageTable *Page_Table, 
                            unsigned int LogicalAddress,
                            unsigned int Frame) {
    bool frame_exists = PageLookup(Page_Table, LogicalAddress) != nullptr;
    if(frame_exists) {
        //don't add anything
    }
    else {
        //add
    }
    return;
}

void PageTable::PageInsert(Level *Level,
                            unsigned int LogicalAddress,
                            unsigned int Frame) {
	return;
}

/**
 * Level is a data structure that is used with PageTable. The amount of
 * levels correspond to the amount of values that the user types in
 * when compiling out program. It's depth represents it's position
 * in relation with other levels and it contains a pointer that
 * will point back to a PageTable pointer. 
 */
Level::Level(int depth, PageTable *ptr) {
	current_depth = depth;
   	page_table_ptr = ptr;
   	int level_count = ptr->get_level_count(ptr);
   	int depth_entry_count = ptr->entry_count[current_depth - 1];
   	bool is_leaf_node = (current_depth == level_count - 1);
   	if(is_leaf_node) {
   		next_level_ptr = nullptr; // There are no more levels after this
   		cout << ptr->entry_count[current_depth - 1] << endl;
   		next_map = new struct Map[sizeof(Map) * depth_entry_count];
   		for(int i = 0; i < ptr->entry_count[current_depth]; i++) {
			next_map[i].frame = 0;
			next_map[i].is_valid_frame = false;
   		}
   		// make map object of size depth -1
   		// set everything to "invalid" aka false
   	} else {
   		next_map = nullptr;
   		next_level_ptr = new Level *[sizeof(void *) * depth_entry_count];
   		for(int i = 0; i < ptr->entry_count[current_depth]; i++) {
   			next_level_ptr[i] = nullptr;
   		}
   		// create a next_level array of size depth -1
   		// set everything in it to nullptr
   	}
    
}
int Level::get_depth() {
    return current_depth;
}

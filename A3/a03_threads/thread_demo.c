/*
* @author           Erick Pulido
* @instructor       Dr. Roch
* @class            CS570 - OPERATING SYSTEMS
* @due              03/12/19
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "worker.h"
#define NUMBER_OF_THREADS 5

int main() {
    int i;
    pthread_t threads[NUMBER_OF_THREADS]; // Array of threads;
    int values[NUMBER_OF_THREADS];       // Array of values to pass in
    for(i = 0; i < NUMBER_OF_THREADS; i++) {
        values[i] = i;
        if(pthread_create(&threads[i], NULL, worker, &values[i]) != 0)
            exit(-1);
    }
    for(i = 0; i < NUMBER_OF_THREADS; i++) {
        if(pthread_join(threads[i], NULL) != 0)
            exit(-1);
    }
    printf("Work complete\n");
    return 0;
}

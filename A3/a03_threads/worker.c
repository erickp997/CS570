/*
* @author           Erick Pulido
* @instructor       Dr. Roch
* @class            CS570 - OPERATING SYSTEMS
* @due              03/12/19
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *worker(void *arg) {
    pthread_mutex_lock(&mutex);
    int number = *(int *) arg; // Treat arg as int
    printf("Square of %d is %d\n", number, number * number);
	pthread_mutex_unlock(&mutex);
    pthread_exit(NULL);
}

/**
 * Anthony Alvidrez
 * Erick Pulido
 * 3/12/19 
 */
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <cstdlib>
#include <iomanip>
#include <vector>
#include <cstdio>
#include "structures.h"
#include "byutr.h"
using namespace std;

/**
 * This method is to use ofstream in order to write to a file
 * depending on if a flag was used with user input. In our main
 * we use a switch statment to see if any flags were used.
 * If they were then we handle each case accordingly.
 */
void write_to_file(char *destination_file, int n_memory_references,
				   std::vector<int> bits, int level) {
    ofstream ofs;
    ofs.open(destination_file, std::ofstream::out|std::ofstream::app);
    ofs << n_memory_references << " -> " << level << endl;
    ofs.close();   
}

void process_addresses(int address_count, int n_memory_references, 
					   char *sourcefile) {
   	// causes smash stack
    FILE *fp = fopen(sourcefile, "rb");
    p2AddrTr address;
    
	if(fp == NULL) {
		cerr << "Error opening file\n" << endl;
		exit(EXIT_FAILURE);
	}
	while((address_count < n_memory_references) && (!feof(fp))) {
        unsigned int logical_address = NextAddress(fp, &address);
        address_count++;
        cout << "Address is " << hex << logical_address << endl;
	}
	fclose(fp);
}

/**
 * The main function acts as our main driver for this program.
 * Here we initialize our main variables that we will
 * use for printing at the end and various sizes in order to
 * create our data structures of the correct size. Here we also 
 * see if any flags were used when excecuting our program.
 * If they were then we use if statments to handle them
 * accordingly. At the very end we make sure to print out our variables
 * in the proper format according to the directions.
 */
int main(int argc, char **argv) {
    int pageSize = 0;
    int hitNum = 0;
    double percentHit = 0.0;
    int missNum = 0;
    double percentMiss = 0.0;
    int addressNum = 0;
    int byteNum = 0;
    int n_flag = 0;								
	int n_memory_references = 0;	// First n memory references
    int p_flag = 0;
    char *optional_output = NULL; 	// Optional output file
    int t_flag = 0;			        // Optional translation display
    const char *flag_string = "n:p:t";
    char *trace_file = NULL;
    int opt;
    int index;

	unsigned int level_count = 0;
	std::vector<int> bits;      // Hold the numerical input of execution
  
    /* Detect optional flags in argument and handle according to case */
    while((opt = getopt(argc, argv, flag_string)) != -1) {
        switch(opt) {
            case 'n':
                n_flag = 1;
				n_memory_references = atoi(optarg);
                break;
            case 'p':
                p_flag = 1;
                optional_output = optarg;
                break;
            case 't':
                t_flag = 1;
                break;
            default:
            	cerr << "Option not supported!" << endl;
            	exit(EXIT_FAILURE);
        }
    }

    for(index = optind; index < argc; index++) {
		if(level_count != 0) 
			bits.push_back(atoi(argv[index])); // Collect bits for each level
		level_count++;
    }    
    
	level_count -= 1; // The loop takes level count one level too far
	int bitmask[level_count];
	int shift_arr[level_count];
	int entry[level_count];
	
    int count = 0;
    for(int i = optind+1; i < argc; i++) { // Populate arrays prior to altering
        bitmask[count] = atoi(argv[i]);
        shift_arr[count] = atoi(argv[i]);
        entry[count] = atoi(argv[i]);
        count++;
    }
    trace_file = argv[optind];

    generate_bitmask(bitmask, level_count);
    generate_shift(shift_arr, level_count);
    generate_entry_count(entry, bits, level_count);
   
    //PageTable *pt = new PageTable(level_count, bitmask, shift_arr, entry);
    
    //loop through file and get next address
    //NextAddress(argv[optind], p2AddrTr *addr_ptr);

    if (n_flag == 1) {
        //process only the first N memory references
    }
    if (p_flag == 1) {
        write_to_file(optional_output, n_memory_references, bits, level_count);
    }
    if (t_flag == 1) {
        //Show the logical to physical address translation
        //for each memory reference
    }
    
    
    //process_addresses(addressNum, n_memory_references, trace_file);

    cout << "Page Size: " << pageSize << endl;
    cout << "Hits " << hitNum << "(" << percentHit << "%), " 
    << "Misses " << missNum << "(" << percentMiss << "%) " << "# Addresses "
    << addressNum << endl;
    cout << "Bytes used: " << byteNum << endl; 
    exit(EXIT_SUCCESS);
}

# Scheduling and memory

> ### You will construct a simulation of an N-level page tree. Your program will read a file consisting of memory accesses for a single process, construct the tree, and assign frame indices to each page. Once all of the addresses have been specified, the pages which are in use will be printed in order. You will assume an infinite number of frames are available and need not worry about a page replacement algorithm. See section functionality for details. 

[Full assignment source](https://github.com/SpurOTM/CS570/blob/master/A3/a3.pdf)

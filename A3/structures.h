/**
 * Anthony Alvidrez
 * Erick Pulido
 * 3/12/19 
 */
#ifndef STRUCTURES_H
#define STRUCTURES_H
#include <vector>
#include <cmath>
class PageTable;
class Level;
class Map;
unsigned int LogicalToPage(unsigned int, unsigned int, unsigned int);
void generate_bitmask(int *, int);
void generate_shift(int *, int);
void generate_entry_count(int *, std::vector<int>, int);
/*
* The top level descriptor of the N level page table and containing a pointer
* to the level 0 page structure.
*/
class PageTable {
    public: //private:
        Level *root_node_ptr;   // Points to the level where current depth is 0
        int level_count;        // Number of levels
        int *bitmask_array;    // Bit mask for level i
        int *shift_array;      // Number of bits to shift level i page bits
        int *entry_count;      // Number of possible pages for level i
    //public:
        /* Constructor builds a PAGETABLE object of specified parameters */
        PageTable(int level, int *bitmask, int *shift, int *entry);

        Map *PageLookup(PageTable *Page_Table, unsigned int LogicalAddress);
        
        int get_level_count(PageTable *Page_Table);

        void PageInsert(PageTable *Page_Table,
                        unsigned int LogicalAddress,
                        unsigned int Frame);

        void PageInsert(Level *Level, 
                        unsigned int LogicalAddress, 
                        unsigned int Frame);

        /* Destructor called at the very end */
        ~PageTable() {}
        
};
/* An entry for the arbitrary level which represents one of the sublevels. */
class Level {
    public:
        int current_depth;
        PageTable *page_table_ptr;
        Level *current;
        Level **next_level_ptr;    
        Map *next_map;   

        Level(int depth, PageTable *ptr);
        int get_depth();
        ~Level() {}
};
struct Map {
	int frame;
	bool is_valid_frame;
};

#endif

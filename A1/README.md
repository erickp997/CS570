# Demonstrate proficiency in C/C++

> ### This program is designed to test your proficiency in C or C++ and demonstrate that you have the prerequisite knowledge in one of these two languages. This assignment must be done by yourself; teams are not allowed

[Full assignment source](https://github.com/SpurOTM/CS570/blob/master/A1/a01.pdf)

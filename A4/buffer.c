#include "buffer.h"
/* pseudo constructor */
void create_conveyer(struct conveyer *conveyer_obj) {
	conveyer_obj->front = NEW_CONVEYER;
	conveyer_obj->rear = NEW_CONVEYER;
}
/* remove everything from the queue */
void clear(struct conveyer *conveyer_obj) {
	conveyer_obj->front = NEW_CONVEYER;
	conveyer_obj->rear = NEW_CONVEYER;
}
/* add item to end of queue */
int enqueue(struct conveyer *conveyer_obj, int item) {
	if(isFull(conveyer_obj)) return 0;
	conveyer_obj->rear++;
	conveyer_obj->array[conveyer_obj->rear % LIMIT] = item;
	return 1;
}
/* remove the first item of the queue */
int dequeue(struct conveyer *conveyer_obj) {
	return isEmpty(conveyer_obj) ? NEW_CONVEYER : conveyer_obj->array[conveyer_obj->front++ % LIMIT];
}
/* Check if the LIMIT of this queue implementation has been reached */
int isFull(struct conveyer *conveyer_obj) {
	return((conveyer_obj->rear - LIMIT) == conveyer_obj->front);
}
/* Check if there is nothing in the queue */
int isEmpty(struct conveyer *conveyer_obj) {
	return conveyer_obj->front == conveyer_obj->rear;
}

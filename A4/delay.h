#ifndef DELAY_H
#define DELAY_H
/* Simple struct to hold the (optional) delays for different candies or
*  consumer in question.
*/
struct delay {
	int delay_frog;
	int delay_escargot;
	int delay_lucy;
	int delay_ethel;
};
void construct_delays(struct delay *);
#endif

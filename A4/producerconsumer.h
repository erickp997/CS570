#ifndef PRODUCERCONSUMER_H
#define PRODUCERCONSUMER_H

#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include "belt.h"
#include "buffer.h"
#include "delay.h"
#include "factory.h"
#define NOT_EMPTY 0
#define NOT_FULL NOT_EMPTY
#define FROG 1
#define ESCARGOT 0
#define FROG_LIMIT 3
#define CANDY_LIMIT 100
#define DELAY_FACTOR 1000

/* Print a message for whether a candy was produced or consumed. */
void current_report(struct belt *, const char *);
/* Print the summary of the day's work in the factory. */
void full_report(struct belt *);
/* Produce a frog candy. */
void frog_producer (struct factory *);
/* Produce an escargot candy. */
void escargot_producer(struct factory *);
/* Candy is consumed by Lucy. */
void lucy_consumer(struct factory *);
/* Candy is consumed by Ethel. */
void ethel_consumer(struct factory *);
#endif

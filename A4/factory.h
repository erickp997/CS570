#ifndef FACTORY_H
#define FACTORY_H
#include <semaphore.h>
#include "belt.h"
#include "delay.h"
/* A simple struct to hold references to the buffer and delay structs which
*  contain further information that the producer and consumer methods
*  need to process the candy. Contains two semaphores as well.
*/
struct factory {
	struct belt *current_belt;
	struct delay *current_delay;
	sem_t candy;
	sem_t barrier;
};

void construct_factory(struct factory *);
#endif

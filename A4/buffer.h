#ifndef CONVEYER_H
#define CONVEYER_H
#include <stdio.h>
#define LIMIT 100 	// Instructions state the buffer can not exceed 100 items
#define NEW_CONVEYER -1
/*
* conveyer will mimick an array based queue as that is one of the choices
* for a buffer that is straightforward and easy to use.
*/
struct conveyer {
	int front;
	int rear;
	int array[LIMIT];
};
/* pseudo constructor */
void create_conveyer(struct conveyer *);
/* remove everything from the queue */
void clear(struct conveyer *);
/* add item to end of queue */
int enqueue(struct conveyer *, int);
/* remove the first item of the queue */
int dequeue(struct conveyer *);
/* Check if the LIMIT of this queue implementation has been reached */
int isFull(struct conveyer *);
/* Check if there is nothing in the queue */
int isEmpty(struct conveyer *);
#endif

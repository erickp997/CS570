#include "producerconsumer.h"
/* Prints a straightforward message of what has just occurred in the producer
*  or consumer method. Mentions the number of frogs, escargots, total candy,
*  number produced and a short message indicating what occurred.
*/
void current_report(struct belt *belt_obj, const char *message) {
	 printf("Belt: %d frogs + %d escargots = %d. produced: %d\t%s\n", 
	 		belt_obj->total_frogs, 
	 		belt_obj->total_escargots, 
	 		(belt_obj->total_frogs + belt_obj->total_escargots), 
	 		belt_obj->total_candies, 
	 		message);
}
/* The end of the day summary for everything that happened in the factory.
*  Shows all the candies produced and who consumed how many of each candy.
*/
void full_report(struct belt *belt_obj) {
	printf("\nPRODUCTION REPORT\n");
	printf("----------------------------------------\n");
	printf("crunchy frog bite producer generated %d candies\n", 
			belt_obj->frog_count);
	printf("escargot sucker producer generated %d candies\n", 
			belt_obj->escargot_count);
	printf("Lucy consumed %d crunchy frog bites + %d escargot suckers = %d\n",
			belt_obj->Lucy_frog_count, belt_obj->Lucy_escargot_count, 
			(belt_obj->Lucy_frog_count + belt_obj->Lucy_escargot_count));
	printf("Ethel consumed %d crunch frog bites + %d escargot suckers = %d\n",
			belt_obj->Ethel_frog_count, belt_obj->Ethel_escargot_count, 
			(belt_obj->Ethel_frog_count + belt_obj->Ethel_escargot_count));
}
/* A crunchy frog bite candy is produced while the specific condition of no
*  more than 3 crunchy frog bite candies is met. A message is printed to 
*  notify this candy has been produced.
*/
void frog_producer(struct factory *factory_obj) {
	const char *message = "Added crunchy frog bite.";
	for(;;) {
		sem_wait(&factory_obj->candy); // lock	
		/* Are there no more than 3 frogs? 
		*  Have we not reached our production limit of 100 candies yet?
		*  Does our buffer have room?
		*/
		if ((factory_obj->current_belt->total_frogs < FROG_LIMIT) &&
			factory_obj->current_belt->total_candies < CANDY_LIMIT &&
			(isFull(factory_obj->current_belt->conveyer_obj) == NOT_FULL)) {

			enqueue(factory_obj->current_belt->conveyer_obj, FROG);
			factory_obj->current_belt->total_frogs++;
			factory_obj->current_belt->total_candies++;
			factory_obj->current_belt->frog_count++;
			current_report(factory_obj->current_belt, message);	
				
		}		
		sem_post(&factory_obj->candy); // unlock
		
		/* Inclusive of last candy on buffer; 100th candy must be produced. */
		if (factory_obj->current_belt->total_candies >= CANDY_LIMIT)
			pthread_exit(EXIT_SUCCESS);
		
		/* Optionally, sleep for N milliseconds. */
		usleep(factory_obj->current_delay->delay_frog);
		//sleep(factory_obj->current_delay->delay_frog / DELAY_FACTOR);
	}
}
/* An escargot sucker candy has been produced. A message is printed to notify
*  this candy has been produced.
*/
void escargot_producer(struct factory *factory_obj) {
	const char *message = "Added escargot sucker.";
	for(;;) {
		sem_wait(&factory_obj->candy); // lock
		if (factory_obj->current_belt->total_candies < CANDY_LIMIT &&
			(isFull(factory_obj->current_belt->conveyer_obj) == NOT_FULL)) {
			
			enqueue(factory_obj->current_belt->conveyer_obj, ESCARGOT);
			factory_obj->current_belt->total_escargots++;
			factory_obj->current_belt->total_candies++;
			factory_obj->current_belt->escargot_count++;
			current_report(factory_obj->current_belt, message);		
			
		}					
		sem_post(&factory_obj->candy); // unlock 
		
		if (factory_obj->current_belt->total_candies >= CANDY_LIMIT) 
			pthread_exit(EXIT_SUCCESS);
		
		/* Optionally, sleep for N milliseconds. */
		usleep(factory_obj->current_delay->delay_escargot);
		//sleep(factory_obj->current_delay->delay_escargot / DELAY_FACTOR);
	}
}
/* Lucy will consume a candy based on whichever one is next on the buffer.
*  It will either be a crunchy frog bite or an escargot sucker and a 
*  message will notify which candy Lucy has consumed.
*/
void lucy_consumer(struct factory *factory_obj) {
	const char *message_frog = "Lucy consumed crunch frog bite.";
	const char *message_escargot = "Lucy consumed escargot sucker.";
	for(;;) {
		sem_wait(&factory_obj->candy); // lock
		if(isEmpty(factory_obj->current_belt->conveyer_obj) == NOT_EMPTY) {
			int current_candy = dequeue(factory_obj->current_belt->conveyer_obj);
			switch(current_candy) { // What kind of candy will be consumed
				case FROG:
					factory_obj->current_belt->total_frogs--;
					factory_obj->current_belt->Lucy_frog_count++;
					current_report(factory_obj->current_belt, message_frog);
					break;
				case ESCARGOT:
					factory_obj->current_belt->total_escargots--;
					factory_obj->current_belt->Lucy_escargot_count++;
					current_report(factory_obj->current_belt, message_escargot);
					break;
				default:
					perror("Unknown candy type detected.\n");
			}
		}		
		sem_post(&factory_obj->candy); // unlock 
		
		if(factory_obj->current_belt->total_candies >= CANDY_LIMIT)
			pthread_exit(EXIT_SUCCESS);
		
		/* Optionally, sleep for N milliseconds. */
		usleep(factory_obj->current_delay->delay_lucy);
		//sleep(factory_obj->current_delay->delay_lucy / DELAY_FACTOR);
	}
}
/* Ethel will consume a candy based on whichever one is next on the buffer.
*  It will either be a crunchy frog bite or an escargot sucker and a 
*  message will notify which candy Ethel has consumed.
*/
void ethel_consumer(struct factory *factory_obj) {
	const char *message_frog = "Ethel consumed crunch frog bite.";
	const char *message_escargot = "Ethel consumed escargot sucker.";
	for(;;) {
		sem_wait(&factory_obj->candy);		
		if(isEmpty(factory_obj->current_belt->conveyer_obj) == NOT_EMPTY) {
			int current_candy = dequeue(factory_obj->current_belt->conveyer_obj);			
			switch(current_candy) {
				case FROG:
					factory_obj->current_belt->total_frogs--;
					factory_obj->current_belt->Ethel_frog_count++;
					current_report(factory_obj->current_belt, message_frog);
					break;
				case ESCARGOT:
					factory_obj->current_belt->total_escargots--;
					factory_obj->current_belt->Ethel_escargot_count++;
					current_report(factory_obj->current_belt, message_escargot);
					break;
				default:
					perror("Unknown candy type detected.\n");
			}
		}		
		sem_post(&factory_obj->candy);
		
		if(factory_obj->current_belt->total_candies >= CANDY_LIMIT)
			pthread_exit(EXIT_SUCCESS);
		
		/* Optionally, sleep for N milliseconds. */
		usleep(factory_obj->current_delay->delay_ethel);
		//sleep(factory_obj->current_delay->delay_ethel / DELAY_FACTOR);
	}
}

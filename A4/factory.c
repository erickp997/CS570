#include "factory.h"
/* Constructor for factory struct that will simply initialize just the
* references to buffer and delay structs, semaphores are handled in main.
*/
void construct_factory(struct factory *factory_obj) {
	factory_obj->current_belt = NULL;
	factory_obj->current_delay = NULL;
}

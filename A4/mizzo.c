/**
* Anthony Alvidrez
* Erick Pulido
* 04/16/19
* This program will be in Anthony's edoras account: cssc1066
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include "buffer.h"
#include "belt.h"
#include "delay.h"
#include "factory.h"
#include "producerconsumer.h"
#define SHARED_SEM 0
#define LOCKED 0
#define UNLOCKED 1

/* Driver will run program along with optional flags indicating delays to 
*  one or more threads. This will create and initialize defined structures
*  and mimick the producer and consumer problem with Lucy and Ethel in the 
*  Mizzo factory. 
*/
int main(int argc, char **argv) {
	
	const char *parse_flag = "E:L:f:e:";
	int Ethel_milliseconds = 0;
	int Lucy_milliseconds = 0;
	int frog_bite_milliseconds = 0;
	int escargot_sucker_milliseconds = 0;
	struct belt *belt_obj = (struct belt *)malloc(sizeof(struct belt));
	struct conveyer *conveyer_obj = (struct conveyer *)malloc(sizeof(struct conveyer));
	struct delay *delay_obj = (struct delay *)malloc(sizeof(struct delay));
	int opt;
	sem_t lock;
	sem_t thread_barrier;
	
	/* Semaphores with initial statuses stated explicitly for our benefit */
	sem_init(&lock, SHARED_SEM, UNLOCKED);
	sem_init(&thread_barrier, SHARED_SEM, LOCKED);
	
	/* Initialize belt and delay structure along with belt pointing to buffer */
	construct_belt(belt_obj);
	belt_obj->conveyer_obj = conveyer_obj;
	construct_delays(delay_obj);
	
	/* Process input flags */
	while((opt = getopt(argc, argv, parse_flag)) != -1) {
		switch(opt) {
			case 'E': // Delay Ethel's thread
				Ethel_milliseconds = atoi(optarg);
				delay_obj->delay_ethel = Ethel_milliseconds;			
				break;
			case 'L': // Delay Lucy's thread
				Lucy_milliseconds = atoi(optarg);
				delay_obj->delay_lucy = Lucy_milliseconds;
				break;
			case 'f': // Delay crunchy frog bite thread
				frog_bite_milliseconds = atoi(optarg);
				delay_obj->delay_frog = frog_bite_milliseconds;
				break;
			case 'e': // Delay escargot sucker thread
				escargot_sucker_milliseconds = atoi(optarg);
				delay_obj->delay_escargot = escargot_sucker_milliseconds;
				break;
			default:
				perror("Option not supported\n");
				exit(EXIT_FAILURE);
		}
	}
	
	/* Initialize main structure that will be passed to all threads */
	struct factory *mizzo_candy = (struct factory *)malloc(sizeof(struct factory));
	mizzo_candy->current_belt = belt_obj;
	mizzo_candy->current_delay = delay_obj;
	mizzo_candy->candy = lock;
	mizzo_candy->barrier = thread_barrier;
	
	/* The factory struct contains all the information needed to be updated
	*  in the producer consumer problem as well as hold that information
	*  for the report outputs.
	*/
	pthread_t frog, escargot, lucy, ethel;
	pthread_create(&frog, NULL, (void *)&frog_producer, mizzo_candy);
	pthread_create(&escargot, NULL, (void *)&escargot_producer, mizzo_candy);
	pthread_create(&lucy, NULL, (void *)&lucy_consumer, mizzo_candy);
	pthread_create(&ethel, NULL, (void *)&ethel_consumer, mizzo_candy);
	
	pthread_join(frog, NULL);
	pthread_join(escargot, NULL);
	pthread_join(lucy, NULL);
	pthread_join(ethel, NULL);
	
	/* Summary of day report showing what went on that 'factory'.*/
	full_report(mizzo_candy->current_belt);
	
	/* Deallocate memory now that it is no longer needed. */
	free(belt_obj);
	free(conveyer_obj);
	free(delay_obj);
	free(mizzo_candy);
	exit(EXIT_SUCCESS);
}

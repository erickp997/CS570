#include "belt.h"
#include "buffer.h"
/* Constructor for belt struct that will simply initialize all values to
*  their respective default values.
*/
void construct_belt(struct belt *belt_obj) {
	belt_obj->conveyer_obj = NULL;
	belt_obj->total_candies = 0;
	belt_obj->total_frogs = 0;
	belt_obj->total_escargots = 0;	
	belt_obj->Lucy_frog_count = 0;
	belt_obj->Ethel_frog_count = 0;
	belt_obj->Lucy_escargot_count = 0;
	belt_obj->Ethel_escargot_count = 0;	
	belt_obj->frog_count = 0;
	belt_obj->escargot_count = 0;
} 

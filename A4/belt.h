#ifndef BELT_H
#define BELT_H
#include "buffer.h"

struct belt {
	struct conveyer *conveyer_obj;
	int total_candies;
	int total_frogs;
	int total_escargots;	
	int Lucy_frog_count;
	int Ethel_frog_count;
	int Lucy_escargot_count;
	int Ethel_escargot_count;	
	int frog_count;
	int escargot_count;
};
void construct_belt(struct belt *);
#endif

#include "delay.h"
/* Constructor for delay struct to simply leave initialize all the delays to
*  their default values. They may be optionally overwritten should the user
*  want to test program with approved options for delays.
*/
void construct_delays(struct delay *delay_obj) {
	delay_obj->delay_frog = 0;
	delay_obj->delay_escargot = 0;
	delay_obj->delay_lucy = 0;
	delay_obj->delay_ethel = 0;
}

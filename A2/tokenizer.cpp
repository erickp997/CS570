#include <iostream>
#include <list>
#include <string>
#include <readline/readline.h>
#include "tokenizer.h"
#include "parser.h"
#define ESCAPEOFFSET 2
using namespace std;
/*
* printList - prints the contents of the linked list in the specified format
* of [{token1},{token2},...,{tokenN}].
*/
void printList(list<string> sample) {
    int count = 0;
    cout << "[";
    for(auto i : sample) {
        count++;
        cout << "{" << i << "}";
        //this ensures no comma is printed after the last token
        if(count < sample.size())
            cout << ",";
    }
    cout << "]";
    cout << "\n";
}
/*
* main will read in a line, tokenize the input and print the list of tokens.
* This can be repeated as many times as the user wishes.
*/
void tokenizer() {

    list <string> list_of_tokens;
    char *test_string;

    //put input in while loop to perform as many times as desired
    while((test_string = readline("> ")) != nullptr) {
        int length = strlen(test_string);
        int i = 0;        
        string token; //characters will be added through checks to make up the token

RECHECK:   while(i < length) {
            if(test_string[i] == '\\') {
                // copy the character after the escape character and offset index
                token += test_string[i + 1];
                i += ESCAPEOFFSET;
            }
            // double quotes have to check for escape and closing quotes
            else if(test_string[i] == '"') {
                i++;
                while(i < length) {
                    if(test_string[i] == '\\') {
                        token += test_string[i + 1];
                        i += ESCAPEOFFSET;
                    }
                    if(test_string[i] == '"') {
                        i++;
                        goto RECHECK; // go check if more tokens follow closing quotes
                    }
                    token += test_string[i++];
                }
            }
            // similar situation as double quotes
            else if(test_string[i] == '\'') {
                i++;
                while(i < length) {
                    if(test_string[i] == '\'') {
                        i++;
                        goto RECHECK;
                    }
                    token += test_string[i++];
                }
            }
            // check if there is a single special character or special and something else
            else if(test_string[i] == '|') {
                if(!token.empty()) {
                    list_of_tokens.push_back(token);
                    token.clear();
                }
                token += test_string[i];
                list_of_tokens.push_back(token);
                token.clear();
                i++;
            }
            else if(test_string[i] == ';') {
                if(!token.empty()) {
                    list_of_tokens.push_back(token);
                    token.clear();
                }
                token += test_string[i];
                list_of_tokens.push_back(token);
                token.clear();
                i++;
            }
            else if(test_string[i] == '<') {
                if(!token.empty()) {
                    list_of_tokens.push_back(token);
                    token.clear();
                }
                token += test_string[i];
                list_of_tokens.push_back(token);
                token.clear();
                i++;
            }
            else if(test_string[i] == '>') {
                if(!token.empty()) {
                    list_of_tokens.push_back(token);
                    token.clear();
                }
                token += test_string[i];
                list_of_tokens.push_back(token);
                token.clear();
                i++;
            }
            else if(test_string[i] == '&') {
                if(!token.empty()) {
                    list_of_tokens.push_back(token);
                    token.clear();
                }
                token += test_string[i];
                list_of_tokens.push_back(token);
                token.clear();
                i++;
            }
            // spaces, tabs, and others are skipped
            else if(isspace(test_string[i])) {
                if(!(token.empty())) {
                    list_of_tokens.push_back(token);
                    token.clear();
                }
                i++;
            }
            // anything not specified is becomes part of a token verbatim
            else {
                token += test_string[i];
                i++;
            }
        }
        // if there is something validated in token we can add to linked list
        if(!token.empty()) list_of_tokens.push_back(token);
        // once the linked list is populated by something we print the 
        // contents and clear it for next round of input
        if(list_of_tokens.size() > 0) {
            execute_commands(list_of_tokens); // Single modification to file
            list_of_tokens.clear();
        }
    }
}

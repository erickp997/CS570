#include <iostream>
#include <unistd.h>
#include <vector>
#include <sys/param.h>
#include <sys/wait.h>
#include <cstring>
#include <list>
#include <string>
#include <iterator>
#include <readline/readline.h>
#include "tokenizer.h"
#include "parser.h"
#define CD_ARGS 3 // CD = 1, [target directory] = 1, NULL = 1
using namespace std;

/*
* Recieves a linked list of tokens and is analyzed for commands that are
* separated by either a ; or |. Any number of commands should be executed
* according to the specs provided on the class website.
* 
* FILE REDIRECTS "<" or ">" and their argument will be stripped. A warning is
* then issued.
* BACKGROUND OPERATOR "&" will be stripped as well. A warning is also issued.
* CHANGE DIRECTORY "cd" will work with only one argument. A warning is printed
* if there exists more than one argument.
* PRINT WORKING DIRECTORY "pwd" is the getcwd system call and will print the
* current working directory.
* ALL OTHER COMMANDS are assumed to be programs that launch and are forked,
* then the parent process must wait for the child process to complete. Messages
* will print due to error in forking or executing the specific program.
*/
void execute_commands(list<string> list) {
    
    /* Declarations */
    int count = 0;
    int idx = 0;
    int argument_count = 0;
    int start = 0;
    int end = 0;
    int i = 0;
    char *arguments[list.size() + 1];
    string tmp = "";
    vector<char *> place_holder;

    /* Make the list string contents into proper types for char * array */
    for(auto item: list) {
        tmp = item; 
        arguments[idx++] = strdup(tmp.c_str());
        argument_count++;
    }
    arguments[list.size()] = {NULL}; // We must 'null terminate' the array

/* Outer while loop will allow us to process as many commands as needed */
RELOOP:
    while(end != argument_count) { 
        i = start;

        /* Add commands to a place holder argument vector except ; and | */
        while(i < argument_count) {
            end++;
            if(strcmp(arguments[i], ";") == 0) { //
                start = end; 
                break;  
            }
            else if(strcmp(arguments[i], "|") == 0) {
                cout << "Pipe not implemented.\n";
                start = end;
                break;
            }
            
            place_holder.push_back(arguments[i]);
            i++;
        }
        place_holder.push_back(NULL); // NULL terminate our argument vector

        vector<char *> temporary = parse(place_holder); // Remove <,>, &
        place_holder.clear(); // Leave it ready for possibly more commands
        vector<char *>::iterator it = temporary.begin();
        string temp = "";
        
        count = 0;
        for(auto item : temporary) {
            count++;
            if(strcmp(item, "cd") == 0) {

                if(temporary.size() > CD_ARGS) { // Error in cd command
                    cout << "Accepts exactly 1 argument.\n";
                    temporary.clear();
                    goto RELOOP;
                }

                else { // Correct use of cd
                    advance(it, count); //point to element after cd command
                    temp = *it;
                    int current_directory = chdir(temp.c_str());
                    if(current_directory != 0) // Invalid directory
                        cout << "Directory does not exist "
                                "or is not accessible.\n";
                    temporary.clear();
                    goto RELOOP;
                }

            }
            else if(strcmp(item, "pwd") == 0) { // Correct use of pwd
                char buffer[MAXPATHLEN]; 
                char *path = getcwd(buffer, MAXPATHLEN);
                cout << path << "\n";
                temporary.clear();
                goto RELOOP;
            }
            else { // Any other command is passed through here
                count = 0;
                fork_wait_exec(temporary);
                temporary.clear();
                goto RELOOP;
            }
        }       
    }
    place_holder.clear(); // Clear just to be safe 
    printList(list);
}

/*
* Given an argument vector: fork the process, have the parent wait for the
* child process to complete. The child uses execvp to try to run the program.
* Messages are printed due to failure in the fork, failure to run the program,
* success in running the program, and the program running with error(s).
*/
void fork_wait_exec(vector<char *> array) {
    pid_t pid = fork();
    if(pid < 0) cout << "Unable to spawn program.\n";
    else if(pid == 0) { // Child process
        char **command = &array[0];
        int command_status = execvp(command[0], command);
        if(command_status < 0) 
            cout << "Unable to execute " << command[0] << "\n";
    }
    //Parent process
    int status;
    if(waitpid(pid, &status, 0) == -1)
        cout << "Process exited with error.\n";
    else
        cout << "Process exited sucessfully.\n";
}
/*
* From the argument vector that is going to be executed, iterate through
* to determine if there are any file redirect symbols or background
* operators. If there are we strip them of that particular argument
* vector.
*/
vector<char *> parse(vector<char *> array) { 
    int count = 0;
    vector<char *>::iterator it_begin = array.begin();
    string temp = "";
    
    for(auto item : array) {
        if(count == array.size() - 1) break; // Avoid comparing NULL
        if(strcmp(item, "<") == 0) {
            advance(it_begin, count);
            array.erase(it_begin); // Delete the redirect symbol
            array.erase(++it_begin); // Delete its argument
            cout << "IO redirection and background not implemented.\n";
            break;
        }
        count++;
    }
    it_begin = array.begin();
    count = 0;
    for(auto item : array) {
        if(count == array.size() - 1) break; // Avoid comparing NULL
        if(strcmp(item, ">") == 0) {
            advance(it_begin, count);
            array.erase(it_begin); // Delete the redirect symbol
            array.erase(it_begin); // Delete its argument
            break;
        }
        count++;
    }
    it_begin = array.begin();
    count = 0;

    for(auto item : array) { 
        if(count == array.size() - 1) break; // Avoid comparing NULL
        if(strcmp(item, "&") == 0) {
            advance(it_begin, count);
            array.erase(it_begin); // Delete background symbol.
            break;
        }
        count++; 
    }
    return array;
}

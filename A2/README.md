# Pair programming, architecture and system calls

> ### In this assignment, you implement the My Underachieving Shell (MUSH) that is based on your previous assignment. The starting point for this assignment is to use your tokenizer to parse out lines read from the user or a redirected file. Your token list should then be analyzed for commands which are separated by command terminators (semicolon and pipe: ; | ). 

[Full assignment source](https://github.com/SpurOTM/CS570/blob/master/A2/a2.pdf)

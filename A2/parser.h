#include <vector>
#ifndef PARSER_H
#define PARSER_H
void execute_commands(std::list<std::string>);
void fork_wait_exec(std::vector<char *>);
std::vector<char *> parse(std::vector<char *>);
#endif
